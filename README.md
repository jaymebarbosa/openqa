![Architecture](https://bitbucket.org/emarx/openqa/downloads/logo_openQA.png)

# Welcome

The use of Semantic Web technologies led to an increasing number of structured data published on the Web. 
Despite the advances on question answering systems retrieving the desired information from structured sources is 
still a substantial challenge. Users and researchers still face difficulties to integrate and compare their systems 
results and performance. *openQA* is an open source question answering framework that unifies approaches from several 
domain experts. The aim of *openQA* is to provide a common platform that can be used to promote advances by easy 
integration and measurement of different approaches. [more](https://bitbucket.org/emarx/openqa/wiki/Home)

## NEWS ##

### openQA RC1, coming soon... ###

## Publications ##

[Towards an Open Question Answering Architecture, Semantics 2014](https://bitbucket.org/emarx/openqa/downloads/public.pdf)