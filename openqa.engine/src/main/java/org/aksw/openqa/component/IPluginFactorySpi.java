package org.aksw.openqa.component;

import java.util.Map;

public interface IPluginFactorySpi<T extends IPlugin> {
	public T create(Map<String, Object> params);
}
