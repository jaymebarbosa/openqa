package org.aksw.openqa.component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;

import org.aksw.openqa.component.providers.impl.ServiceProvider;

/**
 * This class is an abstract implementation of a component provider.
 * 
 * @author http://emarx.org
 *
 * @param <C> A <IComponent> object.
 * @param <F> A <ComponentFactory> of <C>.
 */
public abstract class AbstractPluginProvider<C extends IPlugin, F extends IPluginFactorySpi<C>> extends AbstractProvider<C> {
	
	private static final String DEFAULT_META_PATH = "/";
	private static final String DEFAULT_INIT_SUFFIX = ".ini";
	
	private Map<String, C> components;
	private Map<Class<?>, C> componentClasses;
	private List<C> componentList;
	
	public AbstractPluginProvider() {
		components = new HashMap<String, C>();
		componentClasses = new HashMap<Class<?>, C>();
		componentList = new ArrayList<C>();
	}

	public AbstractPluginProvider(Class<F> clazz, List<? extends ClassLoader> classLoaders, ServiceProvider serviceProvider) {
		this();
		loadComponents(clazz, classLoaders, serviceProvider);
	}
	
	protected void loadComponents(Class<F> clazz, List<? extends ClassLoader> classLoaders, ServiceProvider serviceProvider) {
		List<C> componentList = list(clazz, classLoaders);
		for(C component : componentList) {
			register(component);
		}
	}
	
	public void register(C component) {
		String componentId = component.getId();
		// Do not include an instance, if there is already an instance registered to the component
		if(!components.containsKey(componentId)) {
			this.components.put(componentId, (C) component);
			this.componentList.add((C) component);
			this.componentClasses.put(component.getClass(), (C) component);
		}
	}
	
	public void unregister(C component) {
		String componentId = component.getId();
		this.components.remove(componentId);
		this.componentList.remove(component);
		this.componentClasses.remove(component.getClass());
	}
	
	private List<C> list(Class<F> clazz, ServiceLoader<F> serviceLoader) {
		List<C> list = new ArrayList<C>();
		for (F factory : serviceLoader) {
			Map<String, Object> params = getProperties(clazz, DEFAULT_META_PATH + clazz.getName() + DEFAULT_INIT_SUFFIX);
			list.add(factory.create(params));
		}
		return list;
	}
	
	@Override
	public C get(String id) {
		return components.get(id);
	}
	
	/**
	 * Return a list containing all <IComponent>'s found in the environment.
	 * 
	 * @return a list of all <IComponent>'s found in the environment.
	 */
	public List<C> list() {
		return componentList;
	}
	
	/**
	 * Retrieve a registered <IComponent> same as, or that implements or extends the given
	 * <IComponentProvider> class.
	 * In case there is no <IComponent> for a given class null is returned.  
	 * 
	 * @param clazz the Class of the component that should be returned
	 * @return an activated class of <IComponent> that implements the given class or null 
	 * if the component can not be found or is deactivated.
	 */
	public C get(Class<? extends C> clazz) {
		for(Class<?> classEntry : componentClasses.keySet()) {
			if(clazz.isAssignableFrom(classEntry)) {
				C component = componentClasses.get(classEntry);
				if(component.isActive())
					return component;
			}
		}
		return null;
	}
	
	/**
	 * Instantiate all the existing <F> classes in the given classLoaders.  
	 * 
	 * @param clazz the <F> Class of the objects that should be returned.
	 * @param classLoaders the <ClassLoaders> that will be used to searching for the <F> implementations. 
	 * @return a list containing all instantiated <C> from a given <F> in the given classLoaders.
	 */
	protected List<C> list(Class<F> clazz, List<? extends ClassLoader> classLoaders) {
		List<C> components = new ArrayList<C>();
		for(ClassLoader loader: classLoaders) {
			ServiceLoader<F> serviceLoader = ServiceLoader.load(clazz, loader);
			components.addAll(list(clazz, serviceLoader));
		}
		return components;
    }
}
